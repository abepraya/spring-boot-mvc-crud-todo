package com.maybank.todolist.controller;

import java.sql.Date;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.maybank.todolist.entity.Todo;
import com.maybank.todolist.service.TodoService;

@Controller
@RequestMapping("/todolist")
public class TodoController {

	@Autowired
	private TodoService todoService;

	@RequestMapping
	public String view(Model model, @RequestParam(name = "submit", required = false) String submitStatus) {
		model.addAttribute("todolist", new Todo());
		if (submitStatus != null && submitStatus.equals("1")) {
			model.addAttribute("submitStatus", true);
			return "redirect:todolist/list";
		}

//		List<Todo> todos = this.todoService.getAllTodoList();
//		model.addAttribute("todos", todos);

		return "todolistform";
	}
	
	@RequestMapping("/list")
	public String todoList(Model model, String keyword, Date findDate) {
		List<Todo> todos = this.todoService.getAllTodoList();
		List<Todo> result = this.todoService.sortTodo(keyword);
		List<Todo> resultDate = this.todoService.sortTodoByDate(findDate);
		
		if(keyword != null) {
			model.addAttribute("todos", result);
		}
		else if(findDate != null) {
			model.addAttribute("todos", resultDate);			
		}
		else if(findDate == null || findDate.equals("")){
			model.addAttribute("todos", todos);			
		}
		else {
			model.addAttribute("todos", todos);
		}
		
		return "datatodolist";
	}

	@PostMapping("/process")
	public String processData(@ModelAttribute("todolist") Todo todo) {
//		DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
//		String formatDate = formatter.format(todo.getTarget_date());

//			Date date = formatter.parse(formatDate);
//			todo.setTarget_date(date);
//		System.out.println("time :" + formatDate);
		System.out.println("process id : " + todo.getId());
		this.todoService.save(todo);

		return "redirect:/todolist?submit=1";
	}
	
	@RequestMapping("/edit/{id}")
	public ModelAndView editTodo(@PathVariable(name = "id") Long id) {
		ModelAndView mav = new ModelAndView("updatetodolist");
		System.out.println("check id : " + id);
		Todo todo = this.todoService.getId(id);
		mav.addObject("todolist", todo);
		
		return mav;
	}
	
	@RequestMapping("/delete/{id}")
	public String deleteTodo(@PathVariable(name = "id") Long id) {
		this.todoService.delete(id);
	    return "redirect:/todolist/list";       
	}
	

}
