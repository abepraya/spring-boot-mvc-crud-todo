package com.maybank.todolist.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.maybank.todolist.entity.Machine;
import com.maybank.todolist.repository.MachineRepository;
import com.maybank.todolist.service.MachineService;

@Controller
@RequestMapping("/machine")
public class MachineController {

	@Autowired
	private MachineService machineService;
	
	@RequestMapping
	public String view(Model model, @RequestParam(name = "submit", required = false) String submitStatus) {
		model.addAttribute("machine", new Machine());

		return "addmachine";
	}
	
	@PostMapping("/process")
	public String processData(@ModelAttribute("machine") Machine machine) {
		this.machineService.save(machine);

		return "redirect:/machine";
	}
}
