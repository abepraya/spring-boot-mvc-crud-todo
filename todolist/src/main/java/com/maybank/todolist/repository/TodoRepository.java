package com.maybank.todolist.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.maybank.todolist.entity.Todo;

@Repository
public interface TodoRepository extends JpaRepository<Todo, Long> {
	
	//Custom query
	@Query(value = "select * from todolist td where td.user like %:keyword% or td.description like %:keyword% or td.start_meter like %:keyword%", nativeQuery = true)
	public List<Todo> search(@Param("keyword") String keyword);
	
	@Query(value = "select * from todolist td where td.target_date like %:findDate%", nativeQuery = true)
	public List<Todo> findAllByDate(Date findDate);
}
