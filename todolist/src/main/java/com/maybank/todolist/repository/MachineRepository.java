package com.maybank.todolist.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.todolist.entity.Machine;

public interface MachineRepository extends JpaRepository<Machine, Long> {

}
