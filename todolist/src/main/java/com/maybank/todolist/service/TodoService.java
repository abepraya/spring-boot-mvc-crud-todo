package com.maybank.todolist.service;

import java.sql.Date;
import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.todolist.entity.Todo;
import com.maybank.todolist.repository.TodoRepository;

@Service
@Transactional
public class TodoService {

	@Autowired
	private TodoRepository todoRepository;
	
	public void save(Todo todo) {
		this.todoRepository.save(todo);
	}
	
	public List<Todo> getAllTodoList() {
		return this.todoRepository.findAll();
	}
	
	public List<Todo> sortTodo(String keyword) {
        if (keyword != null) {
            return this.todoRepository.search(keyword);
        }
        return this.todoRepository.findAll();
    }
	
	public List<Todo> sortTodoByDate(Date findDate){
		if(findDate != null) {
			return this.todoRepository.findAllByDate(findDate);
		}
		return this.todoRepository.findAll();
	}
	
	public Todo getId(Long id) {
		return todoRepository.findById(id).get();
	}
	
	public void delete(Long id) {
		todoRepository.deleteById(id);
	}
}
