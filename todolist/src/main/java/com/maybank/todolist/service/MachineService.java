package com.maybank.todolist.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import com.maybank.todolist.controller.List;
import com.maybank.todolist.entity.Machine;
import com.maybank.todolist.repository.MachineRepository;

@Service
@Transactional
public class MachineService {

	@Autowired
	private MachineRepository machineRepository;
	
	public void save(Machine machine) {
		this.machineRepository.save(machine);
	}

//	public List<Machine> getAllMachineList() {
//		
//		return null;
//	}
}
